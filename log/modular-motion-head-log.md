## 2020 08 01

For that power spike, the transient frequency is actually quite high... 500ns 'period', not 15khz: 2mhz. To make a true filter, I should potentially add an R to my C, making a low pass filter. This could pair well with the bleed: if I do a 10k resistor here, I draw 2mA through the 10k, and should use an 80pf (so 100pf) cap to bypass. I'll throw 4 0805 footprints down here, and will start with 10k and 100pf, see how that fares. 

OK, through most of the setup. 

- pick sercom / DE / RE / TERM etc for RS485 bonus 
- pick sercom for i2c display 
- pick sercom for rpi serial 
- label cut-jumpers, config auto 

OK, done. 

## 2021 08 26

Well, going to see about drawing another one of these things. And about a year after the first one, what are the odds. 

What I mostly want is to be able to remote-reset 24v and 5v. I think... this means hi-side gate drivers etc. A bit of an experiment then, but not so bad. Will see... 

So! Would like to be able to leave machines plugged in and turned on, then remotely cycle power. The potential raspberry pi kind of complicates this. 

Let's see, I have 24v that I want to step down to 5v, and have a 2.5a module to get that much 5v power: though if I want to run the whole system on that power, probably want some more overhead. I have this: https://www.pololu.com/product/4092 footprint, that's a 5.5A regulator - enough - and it's $23.5, but I'd love to have standalone machines that work well so here we go. 

The other option would be two of those 2.0a modules: one serves the RPI and the other serves the network w/ 24v power. Though these are $11 each, so not much different. I'm going to pick the big one: it can do actually up to 7A when 24v power is input. 

So then from that 5v, I can solder-jumper to the RPI. With that closed, whenever the system first gets power, it'll boot the pi, but the hi-side switches will keep the bus 5v and 24v off. 

Now I have this loop where I could have 5v injected at the module's USB *via the PI* or *via normal usb* - in either case I want to switch 5v out. One approach is to just use the pololu reg all the time and switch w/ its EN enable pin. But this doesn't work because then I kill the onboard logic as well. 

Let's just check states:

- rpi, 24v in,
    - 24v-5vreg powers pi,
    - pi usb powers D51
    - 24v-5vreg powers bus 5v 
- no pi, 
    - laptop usb powers D51 and bus, 
    - most common to start 
- no pi,
    - laptop usb powers D51,
    - 24v-5vreg powers bus 5v 

Bit of a pickle. So I need to be able to connect / disconnect D51 5v from the bus-5v-input.

I'm going to setup the hi-side circuit, as I think in-schematic this will all make more sense. And then I should design jumpers / pin-header-conn-toggles such that we NAND select 5v-to-bus-switch. 

I think a good switch for 24v is the MIC5014YM hi-side driver, though it's a whole $2... looks like it is made for this. I'll pull a FET from the supply I purchased for BLDC drive a while ago, I have boatloads of those. 

Alright... and I think I can use the same to switch 5v? Should do. 

- in schematic:
    - +24v represents input to board,
    - +24v_ps represents after-hi-side
    - +5v is input to top of hi-side switch, the selection 
    - +5v_ps represents after-hi-side, on the bus 

So now I just want to sort the 5v input problem. 

I think it's three modes: (1) w/o pi, usb to d51 goes thru to bus, or (2) w/ pi UART, 24-5vreg powers pi and d51 and bus, or (3) w/ pi USB, 24-5vreg powers pi and bus, but d51 power comes thru USB... jeez, I have 3 things which can be source / sink, but need to guarantee only one source at a time. The bus is *always* a sink, that's easy. The pi and d51 can be sources or sinks, the vreg is only ever a source. Added confusion is that the pi can supply the D51 with USB power. 

- pi (source / sink)
- d51 (source / sink)
- vreg (source only) 
- bus (sink only)

I can set up a 3x3 header, then select source / sink with those 0.1" jumpers. Can't enforce the "only one source" rule, but can add silk to advise. 

So that'd be most of the schematic? I might want to add:

- a few debug / user buttons ? 
- jumper option to hard-lift 5v and 24v (separately) 
- form factor ? sideways BFC, how to PSU integrate ? 
- silk labels 
    - passives
    - jumper 5v select system 

As for the form factor... the lean-to is actually pretty decent. The RPI integration in this vertical form I think is fine, and if I eventually add a display I can float that above the pair of them, the PSU making a kind of back-plate for the system. 

Ah - close here. Thinking maybe 128x64 OLED is maybe a bit better? ... should just relax and finish this though. 

I need a drain resistor and to decide where the BFC goes - before or after the FETs? Probably before - the inrush could be gnarly. That makes placement a little bit messier... 

Done then, I think. I'll order and could do CAD for a new setup. 

![routed](2021-08-26_routed.png)
![schem](2021-08-26_schematic.png)

Damn - sent it out but I've just noticed that I don't have any bonus bypass capacitors here. There's two 10uF at the gate drivers (for 5v and 24v) but no caps at the switched side... I suspect this will be "fine" but it might be worthwhile to re-do. 

Did the update, have 7 added capacitors. 

## 2021 08 29 

Drawing the CAD now, I am going to get set to do the RPI thing, I have 11mm from the bottom of the RPI PCB to the bottom of this PCB. I'm going to leave off a full display integration for later... probably will just end up using the RPI tangentially to run permanent-ish demos in the CBA basement. 

Then the scheme here is to pin the face of the PSU (backside, I guess: where the four M4 mounts are) with the RPI and this PCB, then mount onto the side M4 sets whatever mount I want: a desk stand with feet (four of them, maybe? kinematic blasphemy), or a pin-to-extrusion setup. 

Alright, this is fine. Not the most beautiful of devices. Onwards.

## 2022 01 09 

Finally - sheesh - bringing this up, so that I can remotely switch 24V and 5V, better resetting system state. This is kind of ... not a necessary piece of kit, but is "nice". Arguably the system should go either to a fab-able SAMD21 board, for accessibility, or to a Teensy, for performance. I suppose the teensy version could be accessible as well...

In any case, code... then a test of the switching stuff... then the fab-step work, and maybe we make the step-tick stuff a little better, i.e. actually issuing steps (?) rather than this floating point work, which is awkwards anyways. Maybe this'll be a little bit fun, and maybe it'll get us to the fab-abble motion system that we want as a stopgap! Or maybe all failure, too soon to tell. 

Arg it seems like something I've done w/ the new endpoint API is bricking this. 

God bless, just a recursive overloaded function that was calling itself, when I meant to call a different version of the same name. Whoop! 

OK I can try a 5v turn-on-off code. I guess I'll build my little one-motor-single-page-demo controller as well, sheesh. 

OK these look setup, I'm going to build an example controller here... or in another repo?

The 5v switch works, so I want a little demo-test for the 24v. 

Also at the control head... some tiny distributed statemachine w/ a front end. I have been thinking about this kind of structure - a UI coupled virtual machine. It'd be a good stopgap, but I think I should focus on the typeset OSAP work & what would be a more complete version of this type of thing. 

In any case, the 24v stress testing. I'll solder one of these heat beds up. This provides 2.3 Ohms, at 24v that's just about 250 watts, 10 amps: the PSU is rated for 350W. I could butcher one of these 30-pin ribbons, or just solder a few leads to the BFC pinout. #2, I think. 

Alright well here it goes ffs. Seems like it works, though I suppose that shouldn't be too surprising, the FETS are rated to like 50A each, there's two, and this is 10A. I think I'll stick some little fets on there anyways. 

- test power draw, heat buildup... 
- watch power on scope, is it chill? 
- test 24v w/ no usb power... 

## Part Purchase

- pololu https://www.pololu.com/product/4091 5vreg 7a 
- hi-side 24v gate drive 576-1233-1-ND 
- more power entry 486-1965-ND and fuse holder 486-1956-ND 

## Next Revision

**2021 08 09**

I get a lot of use out of these and it would be useful if they had a few more features.

- ferrite bead, maybe SMT? inspect gnd plane when something is consuming power (i.e. the A4950 drivers), there's a 20kHz wobble (about +/- 1V!) and a longer mode ~ maybe 1kHz or so. 
    - though datasheets seem to suggest these do well with noise > 1mHz,
    - some app. notes suggest a bead w/ capacitor "behind" it, 
    - maybe just a skookum inductor will do the same thing / do it better, both are chokes, etc 
- label the 5v select jumpers, 
- BFC's should lay flat-ish / off board: these things are a PITA to pack into boxes etc 
- update solder-jump connects, auto closed, 
- label silk components, use 1206 ? 
- update module footprint for new sh* 
- name is 'ucbus-head' 
- 5v and 24v 'pre' and 'post' switch LEDs 
- de-sign new PSU hardware as well, should shroud power connections and locate on/off switch in a decent spot. 