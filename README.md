## PSU Baseboard / Bus Head

This is a circuit & mount for a small power supply and embedded system bus-head for the [uart-clocked bus](https://gitlab.cba.mit.edu/jakeread/ucbus-notes). 

![routed](log/2021-08-26_routed.png)
![schem](log/2021-08-26_schematic.png)

### What's Up:

- uses [the module](https://gitlab.cba.mit.edu/jakeread/ucbus-module)
- breaks out 24V 350W & UCBus on one 2x15 IDC
- second 24V 40W UCBus channel available
- RPI mount available
    - RPI UART lines plumbed
    - 5V 2A regulator footprint to power RPI from 24V PSU
    - I2C Display Module footprint available 
    - BFCs & Filtering available 

### Firmware 

I use this board mostly as the 'coordinator' for modular motion systems, firmware is [here](firmware) and uses platform.io to run, see [this guide](https://mtm.cba.mit.edu/2021/2021-10_microcontroller-primer/fab-platformio/) to build and load code. 

### Shorthand BOM 

- 7x 470R / 120R / LED Choice R 
- 3x 1k 
- 2x 4k7 
- 2x 10k
- 4x 1uF
- 4x 10uF
- 8x LED,
- TVS Diode 
- 2x Hi-Side Gate DRV MIC5014 
- 3x NFET TPW4R008NH
- Optional I2C Display 
- Optional 4x Pushbutton 
- Optional Pololu D36V50F5 Regulator (PSU -> RPI)
- Optional RPI 