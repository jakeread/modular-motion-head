#include <Arduino.h>

#include "indicators.h"
#include "utils_samd51/clock_utils.h"

#include "osape/core/osap.h"
#include "osape/vertices/endpoint.h"

#include "osape_arduino/vp_arduinoSerial.h"

#include "osape_ucbus/vb_ucBusHead.h"
#include "osape_ucbus/ucBusHead.h"

#include "osap_debug.h"

#include "smoothie/SmoothieRoll.h"

// -------------------------------------------------------- SMOOTHIE HANDLES 
// *should* go to smoothieRoll.h, duh 

boolean smoothie_is_queue_empty(void){
  return conveyor->queue.is_empty();
}

boolean smoothie_is_moving(void){
  return (smoothieRoll->actuators[0]->is_moving() 
        || smoothieRoll->actuators[1]->is_moving() 
        || smoothieRoll->actuators[2]->is_moving()
        || smoothieRoll->actuators[3]->is_moving()
        || !smoothie_is_queue_empty());
}

// -------------------------------------------------------- OSAP ENDPOINTS SETUP

OSAP osap("motion-head");

VPort_ArduinoSerial vpUSBSer(&osap, "arduinoUSBSerial", &Serial);

VBus vb_ucBusHead(
  &osap, "ucBusHead",
  &vb_ucBusHead_loop,
  &vb_ucBusHead_send,
  &vb_ucBusHead_cts
);

// -------------------------------------------------------- MOVE QUEUE ENDPOINT 

EP_ONDATA_RESPONSES onMoveData(uint8_t* data, uint16_t len){
  // can we load it?
  if(!conveyor->is_queue_full()){
    DEBUG3PIN_TOGGLE;
    // read from head, 
    uint16_t ptr = 0;
    // feedrate is 1st, 
    chunk_float32 feedrateChunk = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    // get positions XYZE
    chunk_float32 targetChunks[4];
    targetChunks[0] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    targetChunks[1] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    targetChunks[2] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    targetChunks[3] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    // check and load, 
    if(feedrateChunk.f < 0.01){
      DEBUG("ZERO FR");
      return EP_ONDATA_ACCEPT; // ignore this & ack 
    } else {
      // do load 
      float target[3] = {targetChunks[0].f, targetChunks[1].f, targetChunks[2].f };
      //sysError("targets, rate: " + String(target[0], 6) + ", " + String(target[1], 6) + ", " + String(target[2], 6) + ", " + String(feedrateChunk.f, 6));
      planner->append_move(target, SR_NUM_MOTORS, feedrateChunk.f, targetChunks[3].f); // it's mm/sec
      return EP_ONDATA_ACCEPT; 
    }
  } else {
    // await, try again next loop 
    return EP_ONDATA_WAIT;
  }
}

Endpoint moveQueueEp(&osap, "moveQueue", onMoveData);

// -------------------------------------------------------- POSITION ENDPOINT 

EP_ONDATA_RESPONSES onPositionSet(uint8_t* data, uint16_t len){
  // only if it's not moving, 
  if(smoothie_is_moving()){
    return EP_ONDATA_REJECT;
  } else {
    uint16_t ptr = 0;
    chunk_float32 targetChunks[4];
    targetChunks[0] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    targetChunks[1] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    targetChunks[2] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    targetChunks[3] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
    // 
    float set[4] = { targetChunks[0].f, targetChunks[1].f, targetChunks[2].f, targetChunks[3].f };
    // ...
    planner->set_position(set, 4);
    return EP_ONDATA_ACCEPT;
  }
}

Endpoint positionEp(&osap, "positions", onPositionSet);

uint8_t posData[16];

void updatePositions(void){
  // write new pos data periodically, 
  uint16_t poswptr = 0;
  ts_writeFloat32(smoothieRoll->actuators[0]->floating_position, posData, &poswptr);
  ts_writeFloat32(smoothieRoll->actuators[1]->floating_position, posData, &poswptr);
  ts_writeFloat32(smoothieRoll->actuators[2]->floating_position, posData, &poswptr);
  ts_writeFloat32(smoothieRoll->actuators[3]->floating_position, posData, &poswptr);
  positionEp.write(posData, 16);
}

// -------------------------------------------------------- CURRENT SPEEDS

Endpoint speedEp(&osap, "speeds");
uint8_t speedData[16];

void updateSpeeds(void){
  // collect actuator speeds, 
  uint16_t wptr = 0;
  ts_writeFloat32(smoothieRoll->actuators[0]->get_current_speed(), speedData, &wptr);
  ts_writeFloat32(smoothieRoll->actuators[1]->get_current_speed(), speedData, &wptr);
  ts_writeFloat32(smoothieRoll->actuators[2]->get_current_speed(), speedData, &wptr);
  ts_writeFloat32(smoothieRoll->actuators[3]->get_current_speed(), speedData, &wptr);
  speedEp.write(speedData, 16);
}

// -------------------------------------------------------- MOTION STATE EP 

Endpoint motionStateEp(&osap, "motionState");  // 4
uint8_t motionData[1];

void updateMotionState(void){
  if(smoothieRoll->actuators[0]->is_moving() || smoothieRoll->actuators[1]->is_moving() || smoothieRoll->actuators[2]->is_moving() || !smoothie_is_queue_empty()){
    motionData[0] = 1;
  } else {
    motionData[0] = 0;
  }
  motionStateEp.write(motionData, 1);
}

// -------------------------------------------------------- WAIT TIME EP 

EP_ONDATA_RESPONSES onWaitTimeData(uint8_t* data, uint16_t len){
  // writes (in ms) how long to wait the queue before new moves are executed 
  // i.e. stack flow hysteresis 
  uint32_t ms;
  uint16_t ptr = 0;
  ts_readUint32(&ms, data, &ptr);
  conveyor->setWaitTime(ms);
  //sysError("set wait " + String(ms));
  return EP_ONDATA_ACCEPT;
}

Endpoint waitTimeEp(&osap, "waitTime", onWaitTimeData);  // 5 

// -------------------------------------------------------- ACCEL SETTTINGS 

EP_ONDATA_RESPONSES onAccelSettingsData(uint8_t* data, uint16_t len){
  // should be 4 floats: new accel values per-axis 
  uint16_t ptr = 0;
  chunk_float32 targetChunks[4];
  targetChunks[0] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[1] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[2] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[3] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  // they're in here 
  for(uint8_t m = 0; m < SR_NUM_MOTORS; m ++){
    smoothieRoll->actuators[m]->set_accel(targetChunks[m].f);
  }
  // assuming that went well, 
  return EP_ONDATA_ACCEPT;
}

Endpoint accelSettingsEp(&osap, "accelSettings", onAccelSettingsData);

// -------------------------------------------------------- RATES SETTINGS 

EP_ONDATA_RESPONSES onRateSettingsData(uint8_t* data, uint16_t len){
  // should be 4 floats: new accel values per-axis 
  uint16_t ptr = 0;
  chunk_float32 targetChunks[4];
  targetChunks[0] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[1] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[2] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[3] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  // they're in here 
  for(uint8_t m = 0; m < SR_NUM_MOTORS; m ++){
    smoothieRoll->actuators[m]->set_max_rate(targetChunks[m].f);
  }
  // assuming that went well, 
  return EP_ONDATA_ACCEPT;
}

Endpoint rateSettingsEp(&osap, "rateSettings", onRateSettingsData);

// -------------------------------------------------------- SPU SETTINGS 

EP_ONDATA_RESPONSES onSPUData(uint8_t* data, uint16_t len){
  // also 4 floats, steps-per-unit... 
  uint16_t ptr = 0;
  chunk_float32 targetChunks[4];
  targetChunks[0] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[1] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[2] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  targetChunks[3] = { .bytes = { data[ptr ++], data[ptr ++], data[ptr ++], data[ptr ++] } };
  // more motor stuff 
  for(uint8_t m = 0; m < SR_NUM_MOTORS; m ++){
    smoothieRoll->actuators[m]->set_steps_per_mm(targetChunks[m].f);
  }
  // recheck max rates 
  smoothieRoll->checkMaxRates();
  // probably all OK 
  return EP_ONDATA_ACCEPT;
}

Endpoint spuEp(&osap, "stepsPerUnitSettings", onSPUData);

// -------------------------------------------------------- POWER MODES 

#define V5_ON PIN_HI(0, 11)
#define V5_OFF PIN_LO(0, 11)
#define V5_SETUP PIN_SETUP_OUTPUT(0, 11); PIN_LO(0, 11)
#define V24_ON PIN_HI(0, 10)
#define V24_OFF PIN_LO(0, 10)
#define V24_SETUP PIN_SETUP_OUTPUT(0, 10); PIN_LO(0, 10)

// 5V Switch on PA11, 24V Switch on PA10
/*  5v  | 24v | legal 
    0   | 0   | yes
    1   | 0   | yes 
    0   | 1   | no 
    1   | 1   | yes 

lol, pretty easy I guess: just no 24v when no 5v...
we also want to turn on in-order though: 5v first, then 24v, and 24v off, then 5v 
*/

// track states 
boolean state5V = false;
boolean state24V = false;

void publishPowerStates(void);

// make changes 
void powerStateUpdate(boolean st5V, boolean st24V){
  // guard against bad state
  if(st24V && !st5V) st24V = false;
  // check order-of-flip... if 5v is turning off, we will turn 24v first, 
  // in all other scenarios, we flip 5v first 
  if(state5V && !st5V){
    state24V = st24V; state5V = st5V;
    // publish, 24v first, and allow charge to leave... 
    state24V ? V24_ON : V24_OFF;
    delay(50);
    state5V ? V5_ON : V5_OFF;
  } else {
    state24V = st24V; state5V = st5V;
    // publish, 5v first, and allow some bring-up... 
    state5V ? V5_ON : V5_OFF;
    delay(10);
    state24V ? V24_ON : V24_OFF;
  }
  // now ... would like to write to the endpoint 
  publishPowerStates();
}

EP_ONDATA_RESPONSES onPowerData(uint8_t* data, uint16_t len){
  // read requested states out 
  boolean st5V, st24V;
  uint16_t rptr = 0;
  ts_readBoolean(&st5V, data, &rptr);
  ts_readBoolean(&st24V, data, &rptr);
  // run the update against our statemachine 
  powerStateUpdate(st5V, st24V);
  // here's a case where we'll never want to let senders to 
  // update our internal state, so we just return 
  return EP_ONDATA_REJECT;
  // this means that the endpoint's data store will remain unchanged (from the write) 
  // but remains true to what was written in when we updated w/ the powerStateUpdate fn... 
}

Endpoint powerEp(&osap, "powerSwitches", onPowerData);

void publishPowerStates(void){
  uint8_t powerData[2];
  uint16_t wptr = 0;
  ts_writeBoolean(state5V, powerData, &wptr);
  ts_writeBoolean(state24V, powerData, &wptr);
  powerEp.write(powerData, 2);
}

// -------------------------------------------------------- SETUP 

void setup() {
  ERRLIGHT_SETUP;
  CLKLIGHT_SETUP;
  DEBUG1PIN_SETUP;
  DEBUG2PIN_SETUP;
  DEBUG3PIN_SETUP;
  DEBUG4PIN_SETUP;
  DEBUG5PIN_SETUP;
  // setup the power stuff 
  V5_SETUP;
  V24_SETUP;
  powerStateUpdate(false, false);
  // osap
  vpUSBSer.begin();
  vb_ucBusHead_setup();
  // smoothie (and frequency of loop below)
  smoothieRoll->init(SR_TICK_FREQ);
  smoothieRoll->checkMaxRates();
  // 25kHz base (40us period) or 
  // 20kHz base (50us period)
  // 10kHz base (100us period) 
  // 5kHz base (200us period)
  d51ClockUtils->start_ticker_a(1000000/SR_TICK_FREQ); 
  // test... 
  // powerStateUpdate(true, false);
}

unsigned long epUpdateInterval = 100; // ms 
unsigned long lastUpdate = 0;

void loop() {
  // main recursive osap loop:
  osap.loop();
  // smoothie checks / etc:
  conveyor->on_idle(nullptr);
  // run 10Hz endpoint update:
  if(millis() > lastUpdate + epUpdateInterval){
    DEBUG5PIN_TOGGLE;
    lastUpdate = millis();
    updatePositions();
    updateSpeeds();
    updateMotionState();
  }
} // end loop 

// runs on period defined by timer_a setup: 
volatile uint32_t timeTick = 0;
volatile uint64_t timeBlink = 0;


volatile uint8_t cachePtr = 0;
uint8_t stepCache[2];             // xyze: step & dir (8 bit) for two cycles 

uint16_t blinkTime = 1000;

uint8_t motion_packet[64]; // three floats bb, space 

void TC0_Handler(void){
  // runs at period established above... 
  TC0->COUNT32.INTFLAG.bit.MC0 = 1;
  TC0->COUNT32.INTFLAG.bit.MC1 = 1;
  DEBUG1PIN_HI;
  
  // do bus action first: want downstream clocks to be deterministic-ish
  ucBusHead_timerISR();

  // do step tick 
  smoothieRoll->step_tick();
  // cache ticks 
  stepCache[cachePtr] = smoothieRoll->actuators[3]->get_step_mask() << 6 | 
                        smoothieRoll->actuators[2]->get_step_mask() << 4 |
                        smoothieRoll->actuators[1]->get_step_mask() << 2 |
                        smoothieRoll->actuators[0]->get_step_mask();
  cachePtr ++;

  if(cachePtr > 1){
    DEBUG2PIN_TOGGLE;
    ucBusHead_transmitA(stepCache, 2);
    cachePtr = 0;
  }

  /*
  // every n ticks, ship position? 
  // each of these ticks drops 10 data bytes, so if we have 17 byte packet, can do every 2nd packet 
  // which would occupy the full bus - notgood - or we can do every 3rd... I'll pick every 4th. 
  timeTick ++;
  if(timeTick > 2){
    DEBUG2PIN_HI;
    timeTick = 0;
    uint16_t mpptr = 0; // motion packet pointer 
    if(planner->do_set_position){
      motion_packet[mpptr ++] = UB_AK_SETPOS;
      planner->do_set_position = false;
    } else {
      motion_packet[mpptr ++] = UB_AK_GOTOPOS;
    }
    // XYZE 
    ts_writeFloat32(smoothieRoll->actuators[0]->floating_position, motion_packet, &mpptr);
    ts_writeFloat32(smoothieRoll->actuators[1]->floating_position, motion_packet, &mpptr);
    ts_writeFloat32(smoothieRoll->actuators[2]->floating_position, motion_packet, &mpptr);
    ts_writeFloat32(smoothieRoll->actuators[3]->floating_position, motion_packet, &mpptr);
    // write packet, put on ucbus
    ucBusHead_transmitA(motion_packet, 17);
    DEBUG2PIN_LO;
  }
  */
  
  // do blinking, lol
  timeBlink ++;
  if(timeBlink > blinkTime){
    CLKLIGHT_TOGGLE;
    sysErrLightCheck();
    timeBlink = 0; 
  }
  DEBUG1PIN_LO;
}