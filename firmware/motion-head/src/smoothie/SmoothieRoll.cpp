/*
SmoothieRoll.cpp

bottle & state container for the SmoothieWare instance running here

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "SmoothieRoll.h"
#include "./osap_config.h"
#ifdef OSAP_DEBUG
#include "./osap_debug.h"
#endif 

SmoothieRoll* SmoothieRoll::instance = 0;

SmoothieRoll* SmoothieRoll::getInstance(void){
    if(instance == 0){
        instance = new SmoothieRoll();
    }
    return instance;
}

SmoothieRoll* smoothieRoll = SmoothieRoll::getInstance();

SmoothieRoll::SmoothieRoll(void){
    
}

void SmoothieRoll::init(float frequency){
    // make motors 
    for(uint8_t m = 0; m < SR_NUM_MOTORS; m ++){
        actuators[m] = new StepInterface();
    }
    stepTicker->init(frequency);
    stepTicker->start();
    conveyor->on_module_loaded();
    conveyor->start(SR_NUM_MOTORS);
}

void SmoothieRoll::step_tick(void){
    stepTicker->step_tick();
}

void SmoothieRoll::checkMaxRates(void){
    // we have frequency, SR_TICK_FREQ, and period which is 1000000/SR_TICK_FREQ 
    // we also have per-axis steps per unit, and max rates, 
    // if rate * spu exceeds the tick frequency, we are in trouble:
    for(uint8_t m = 0; m < SR_NUM_MOTORS; m ++){
        float maxTick = actuators[m]->get_max_rate() * actuators[m]->get_steps_per_mm();
        if(maxTick > SR_TICK_FREQ){
            // new rate should be old rate * rate / 
            float newMax = actuators[m]->get_max_rate() * (SR_TICK_FREQ / maxTick);
            #ifdef OSAP_DEBUG
            DEBUG("motor " + String(m) + " exceeds max tick, " + String(maxTick));
            DEBUG("old max velocity: " + String(actuators[m]->get_max_rate()) + " new: " + String(newMax));
            #endif 
            actuators[m]->set_max_rate(newMax);
        }
    }
}